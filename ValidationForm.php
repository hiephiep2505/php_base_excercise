<?php
$emailError = '';
$emailValid = '';
$passwordError = '';
$passwordValid = '';

if(isset($_POST['submit'])){

    $email = $_POST['email'];
    $password = $_POST['password'];

    if(empty($email)){
        $emailError = 'Email is required';
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailError = 'Email is not valid';
    }else {
        $emailValid = 'Email is valid';
    }

    // if(empty($password)){
    //     $passwordError = 'Password is required';
    // }elseif(strlen($password) < 8) {
    //     $passwordError = 'Password must be at least 8 characters';
    // }elseif(!preg_match('/^[A-Za-z0-9]+$/', $password)) {
    //     $passwordError = 'Passwords must only include uppercase, lowercase letters and numbers';
    // }else {
    //     $passwordValid = 'Password is valid';
    // }
    if(!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\^$*.\[\]{}\(\)?\-\"!@#%&\/,><\':;|_~`])\S{8,99}$/', $password)) {
        $passwordError = 'Password must be at least 8 characters, include uppercase, lowercase letters , numbers and special characters.';
    }else {
        $passwordValid = 'Password is valid';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Validation</title>
</head>
<body>
    <div class="my_form" style="text-align: center; margin-top: 200px">
    <h2>My Form</h2>
    <form action="" method="post" auto_complete="off">
        Email : <input type="text" name="email" id="">
        <div class="error" style="color: red"><?php echo $emailError; ?></div>
        <div class="valid" style="color: green"><?php echo $emailValid; ?></div>
        <br>
        Password : <input type="text" name="password" id="">
        <div class="error" style="color: red"><?php echo $passwordError; ?></div>
        <div class="valid" style="color: green"><?php echo $passwordValid; ?></div>
        <br>
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
    </div>
</body>
</html>