<?php
function drawTriangle() {
    echo 'Tam Giac';
    echo "\n";
    $rows = 4;
    for ($i = 1; $i <= $rows; $i++) {
        for ($j = 1; $j <= $i; $j++) {
          echo "* ";
        }
        echo "\n";
      }
}

function drawSquare() {
    echo 'Hinh Vuong';
    echo "\n";
    $rows = 4;
    for ($i = 1; $i <= $rows; $i++) {
        for ($j = 1; $j <= $rows; $j++) {
          echo "* ";
        }
        echo "\n";
      }
}

function drawEmptySquare() {
    echo 'Hinh Vuong Rong';
    echo "\n";
    $rows = 4;
    for ($i = 1; $i <= $rows; $i++){
        for($j =1; $j <= $rows; $j++){
            if($i == 1 || $i == $rows || $j == 1 || $j == $rows){
                echo "* ";
            }else {
                echo "  ";
            }
        }
        echo "\n";
    }
}

function drawInvertedTriangle() {
    echo 'Tam Giac Nguoc';
    echo "\n";
    $rows = 5;
    for ($i = $rows; $i >=1 ; $i--) {
        for ($j = 1; $j < 2* $rows; $j++) {
            if(abs($rows - $j) <= ($i - 1)) {
                echo "*"; 
            }else {
                echo " ";
            }
        }
      echo "\n";
    }
}

function drawEmptyTriangle() {
    echo 'Tam Giac Rong';
    echo "\n";
    $rows = 4;
    for($i = 1; $i <= $rows; $i++){
        for($j =1; $j < 2*$rows; $j++){
            if(abs($rows - $j) == $i -1 || $i == $rows){
                echo "*";
            }else {
                echo " ";
            }
        }
        echo "\n";
    }
}

drawTriangle();
drawSquare();
drawEmptySquare();
drawInvertedTriangle();
drawEmptyTriangle();

